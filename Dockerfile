FROM docker.io/library/python
RUN pip install PyYAML requests
COPY main.py /opt
WORKDIR /workspace
ENTRYPOINT ["python", "/opt/main.py"]