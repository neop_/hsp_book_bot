#!/usr/bin/env python

import sys
import json
import yaml
import requests
import datetime
import argparse


# fine as global because this is truly global state (although only refresh_token should mutate it), even in parallel
token = None
# immutable -> fine
# also these permeate the entire program
# also lets just see if it's a mistake
args = None
config = None



def main():
    def now():
        # astimezone w/o args assumes local tz (https://docs.python.org/3/library/datetime.html?highlight=now#datetime.datetime.astimezone)
        return datetime.datetime.now().astimezone()

    def refresh_token():
        global token
        response = requests.post('https://api.sport.uni-goettingen.de/token', headers={
            'Accept': 'application/json',
            'Referer': 'https://store.sport.uni-goettingen.de/',
            'Content-Type': 'application/x-www-form-urlencoded',
        }, data={
            'grant_type': 'password',
            'client_id': 'public',
            'UserName': args.user,
            # args.pass is a syntax error
            'Password': vars(args)["pass"]
        })

        response.raise_for_status()
        token = response.json()
        json.dump(token, open("token.json", "w"))

    def run():
        def authorized_request(url, headers, method="GET"):
            headers.update(Authorization=f'Bearer {token["access_token"]}')
            response = requests.request(method, url, headers=headers)
            if response.status_code == requests.codes.unauthorized:
                print(f"unauthorized, trying again with fresh token\n")
                refresh_token()
                headers.update(Authorization=f'Bearer {token["access_token"]}')
                return requests.request(method, url, headers=headers)
            
            return response

        def get_all_appointments_for_allowed_courses():
            def get_appointments_for_course(course_id):
                response = requests.get(f"https://api.sport.uni-goettingen.de/api/course/{course_id}/appointmentsgrouped", headers={
                    'Accept': 'application/json',
                    'Referer': 'https://store.sport.uni-goettingen.de/'
                })
                response.raise_for_status()
                return response.json()

            def get_allowed_courses(sport):
                def get_courses(sport):
                    response = requests.get(f'https://api.sport.uni-goettingen.de/api/sport/{sport}/courses', headers={
                        'Accept': 'application/json',
                        'Referer': 'https://store.sport.uni-goettingen.de/'
                    })
                    response.raise_for_status()
                    return response.json()
                
                def is_course_allowed(course):
                    return course["CourseID"] not in config["CourseID_blacklist"]

                return filter(is_course_allowed, get_courses(sport))


            return (
                appointment
                for sport in config["sports"]
                    for course in get_allowed_courses(sport)
                        for appointment in get_appointments_for_course(course_id=course["CourseID"])
            )
            
        def book(appointments):
            for appointment in appointments:
                response = authorized_request(f'https://api.sport.uni-goettingen.de/api/checkout/courseappointment/{appointment["AppointmentID"]}', headers={
                    'Accept': 'application/json',
                    'Referer': 'https://store.sport.uni-goettingen.de/'
                }, method="POST")
                if response.status_code == requests.codes.created:
                    print(f"registered {args.user} for appointment {json.dumps(appointment)}\n")
                    config["AppointmentID_blacklist"].append(appointment["AppointmentID"])
                    yaml.dump(config, open(args.config, "w"), default_flow_style=False)
                else:
                    print(f"failed to register {args.user} for {json.dumps(appointment)}. Response: {response.text}\n")
        

        def filter_bookable(appointments):
            def get_reservations():
                response = authorized_request('https://api.sport.uni-goettingen.de/api/customer/reservations/', headers={
                    'Connection': 'keep-alive',
                    'Accept': 'application/json',
                    'Origin': 'https://store.sport.uni-goettingen.de',
                    'Referer': 'https://store.sport.uni-goettingen.de/'
                })
                response.raise_for_status()
                return response.json()

                
            # captured in the following function
            reserved_appointment_ids = set((reservation["AppointmentID"] for reservation in get_reservations()))
            def is_bookable(appointment):
                # does the book request have a chance of succeeding?
                return (
                    appointment["Free"] > 0 and
                    datetime.datetime.fromisoformat(appointment["RegisterStart"]) < now() and
                    datetime.datetime.fromisoformat(appointment["Start"        ]) > now() and
                    appointment["AppointmentID"] not in reserved_appointment_ids
                )

            return filter(is_bookable, appointments)

        def remove_stale_appointment_ids_from_denylist(fresh_appointments):
            fresh_appointment_ids = (ap["AppointmentID"] for ap in fresh_appointments)
            config["AppointmentID_blacklist"] = list(set(fresh_appointment_ids) & set(config["AppointmentID_blacklist"]))


        def is_allowed(appointment):
            return appointment["AppointmentID"] not in config["AppointmentID_blacklist"]

        all_appointments = list(get_all_appointments_for_allowed_courses())
        remove_stale_appointment_ids_from_denylist(all_appointments)
        book(filter(is_allowed, filter_bookable(all_appointments)))





    def get_args():
        parser = argparse.ArgumentParser()
        parser.add_argument("--user", type=str, required=True)
        parser.add_argument("--pass", type=str, required=True)
        parser.add_argument("--config", type=str, default="config.yml")
        return parser.parse_args()

    global args
    args = get_args()
    global config
    config = yaml.safe_load(open(args.config))

    global token
    try:
        token = json.load(open(config["token_store"]))
    except FileNotFoundError as err:
        refresh_token()

    print(f"{now()}: starting run\n")
    run()
    print(f"{now()}: finished run\n")



if __name__ == "__main__":
    sys.exit(main())
