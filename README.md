# HSP book bot
This is a program to automatically register you to your sports courses over at https://my.sport.uni-goettingen.de/. It is intended to be executed periodically by you, for example as a cron job on an always-on SBC or by forking this and configuring a CI schedule.

## Usage:
`python3 main.py --help` or `docker run --rm -it hsp_book_bot --help` respectivey should help you with the available command line options. The program needs read access to your config.yml and write access to a folder/file to maintain an authentication token. An example for the config file is in this repository under [config.yml](config.yml). The script registers you for all available appointments that are not blacklisted, from all courses that are not blacklisted, from all sports you selected. Consequently, if you want to deregister from an appointment you should first add it to the appointment blacklist or pause your schedule and only then deregister online. A CLI option to do both for you is planned.

## Deployment:
### Server:
The following is an example setup. I do it similar to this on a BananaPi. Most things are customizable.

I have the following folder structure on my server. Make sure no other user has read or execute permissions.
```
$ tree hsp_bot_data
hsp_bot_data/
├── person0
│   ├── config.yml
│   └── token.json
├── person1
│   ├── config.yml
│   └── token.json
├── log.log
└── run.sh
```
I have this run script to simplify the crontab entry. For this to work you need a container runtime on the machine (eg. docker, podman) and the image needs to be tagged `hsp_book_bot:latest`.
You can build the image by execution `docker build -t hsp_book_bot .` in this repository.
```
$ cat run.sh
#!/bin/bash
/usr/bin/docker run --rm -v /path/to/hsp_bot_data/person0/:/workspace hsp_book_bot --user "foo@stud.uni-goettingen.de" --pass "hunter2"
/usr/bin/docker run --rm -v /path/to/hsp_bot_data/person1/:/workspace hsp_book_bot --user "bar@stud.uni-goettingen.de" --pass "hunter3"
```
This crontab entry runs the above script 5 minutes past every full hour.
```
$ crontab -l
# m h  dom mon dow   command
5 * * * * /path/to/run.sh >>/path/to/log.log 2>&1
```

