### notes:
- run every ten minutes using cron
- get new token and try again once everytime unauthorized
- save token to disk

### process:
1. get courses and all appointments
1. get reservations
1. get appointments for which:
	- [x] you can register timewise
	- [x] not in list of reservations
	- [x] course id not blacklisted
	- [x] appointment id not blacklisted
	- [x] free > 0
1. try to register (last step)